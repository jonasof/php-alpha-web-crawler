<!DOCTYPE HTML>
<html>
<head>
</head>
<body>
    <?php 
    
        set_time_limit ( 60*10 );
        
        error_reporting(0);
        
        //robozinho
        $links_usados = array();
        $urls_usados = array();
        
        $lines_of_file = number_lines_links();
        
        for ($x = $lines_of_file-2; $x > 0; $x--)
            get_arvore_links(get_last_link($x));

        function get_links_page ($url) {
            $html = get_content($url);
            $dom = new DOMDocument;
            @$dom->loadHTML($html);
            $links = $dom->getElementsByTagName('a');
            $array_links = array();
            foreach ($links as $link){
                $array_links[] = $link->getAttribute('href');
            }
            return $array_links;
        }
    
        function get_arvore_links ($url_start, $nivel = 0) {
            global $dominios_usados;
            global $links_usados;
            $links = get_links_page($url_start);
            
            $nivel++;
            
            foreach ($links as $url) {
                if (! in_array(get_domain($url), $dominios_usados)) {
                    $dominios_usados[] = get_domain($url);
                    echo "<p> <a href='" . ($url) . "'>" . get_domain($url) . " - Nivel $nivel</a></p>";
                    flush();
                    add_domain(get_domain($url));
                    add_link($url);
                }
            } 
            
            foreach ($links as $url) {
                if (! in_array($url, $links_usados)) {
                    $links_usados[] = $url;
                    get_arvore_links ($url, $nivel);
                }
            } 
        }
        
        function get_content($URL){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $URL);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;
        }
        
        function get_domain ($url) {
            $parse = parse_url($url);
            return $parse['host'];
        }
        
        function add_domain ($domain) {
            $handle = fopen("dominios", "a");
            fwrite($handle, $domain . "\n");
            fclose($handle);
        }
        
        function add_link ($link) {
            $handle = fopen("links", "a");
            fwrite($handle, $link . "\n");
            fclose($handle);
        }
        
        function number_lines_links() {
            $file="links";
            $linecount = 0;
            $handle = fopen($file, "r");
            while(!feof($handle)){
              $line = fgets($handle);
              $linecount++;
            }

            fclose($handle);

            return $linecount;
        }
        
        function get_last_link($number = 5) {
            $linha = $number;
            
            $file="links";
            $handle = fopen($file, "r");
            for ($x = 1; $x < $linha; $x++) {
                $line = fgets($handle);
            }

            fclose($handle);

            echo $line;
            return $line;
        }
        
    ?>
</body>
</html>